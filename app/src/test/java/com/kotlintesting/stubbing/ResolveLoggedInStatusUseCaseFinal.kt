package com.kotlintesting.stubbing

class ResolveLoggedInStatusUseCaseFinal(
  private val tokenStorage: SharedPreferencesTokenStorage
) {
  fun userLoggedIn(): Boolean {
    return tokenStorage.read() != null
  }
}