package com.kotlintesting.stubbing

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import org.mockito.Mockito

/**
 * Created by jaroslawmichalik
 */
class ResolveLoggedInStatusUseCaseTest {
  @Test
  fun `it should return true on non null token`() {
    //arrange
    val mockTokenStorage: TokenStorage =
      Mockito.mock(TokenStorage::class.java)

    Mockito.`when`(mockTokenStorage.read())
      .thenReturn(TokenEntity(("asd123")))

    val useCase = ResolveLoggedInStatusUseCase(
      tokenStorage = mockTokenStorage
    )

    //act
    val userLoggedIn = useCase.userLoggedIn()

    //assert
    userLoggedIn shouldBe true
  }

  @Test
  fun `it should return false on null token`() {
    //arrange
    val mockTokenStorage: TokenStorage =
      Mockito.mock(TokenStorage::class.java)

    Mockito.`when`(mockTokenStorage.read())
      .thenReturn(null)

    val useCase = ResolveLoggedInStatusUseCase(
      tokenStorage = mockTokenStorage
    )

    //act
    val userLoggedIn = useCase.userLoggedIn()

    //assert
    userLoggedIn shouldBe false
  }
}

