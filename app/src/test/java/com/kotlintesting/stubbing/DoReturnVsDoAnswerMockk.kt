package com.kotlintesting.stubbing

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import kotlin.random.Random

class DoReturnVsDoAnswerMockk {
  @Test
  fun `it should return`() {
    val timeProvider = mockk<RandomNumberProvider> {
      every { random() } returns Random.nextInt()
    }

    repeat(10) {
      println("Return " + timeProvider.random())
    }
  }

  @Test
  fun `it should answer`() {
    val timeProvider = mockk<RandomNumberProvider> {
      every { random() } answers {
        Random.nextInt()
      }
    }

    repeat(10) {
      println("Answer " + timeProvider.random())
    }
  }
}