package com.kotlintesting.stubbing

import android.content.SharedPreferences
import androidx.core.content.edit

class SharedPreferencesTokenStorage(
  private val sharedPreferences: SharedPreferences
) {

  fun update(tokenEntity: TokenEntity) {
    sharedPreferences.edit(commit = true) {
      putString(TOKEN_KEY, tokenEntity.token)
    }
  }

  fun read(): TokenEntity? {
    val token = sharedPreferences.getString(TOKEN_KEY, "")

    return token?.let(::TokenEntity)
  }

  fun delete() {
    sharedPreferences.edit(commit = true) {
      remove(TOKEN_KEY)
    }
  }

  companion object {
    const val TOKEN_KEY = "TOKEN_KEY"
  }
}