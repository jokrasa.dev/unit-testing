package com.kotlintesting.stubbing

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

class ResolveLoggedInStatusUseCaseTestWithFakes {

  @Test
  fun `it should return true on non null token`() {
    //arrange
    val tokenStorage = FakeTokenStorage(
      tokenEntity = TokenEntity("asd123")
    )

    val useCase = ResolveLoggedInStatusUseCase(
      tokenStorage = tokenStorage
    )

    //act
    val userLoggedIn = useCase.userLoggedIn()

    //assert
    userLoggedIn shouldBe true
  }

  @Test
  fun `it should return false on null token`() {
    //arrange
    val tokenStorage = FakeTokenStorage(
      tokenEntity = null
    )

    val useCase = ResolveLoggedInStatusUseCase(
      tokenStorage = tokenStorage
    )

    //act
    val userLoggedIn = useCase.userLoggedIn()

    //assert
    userLoggedIn shouldBe false
  }
}

class FakeTokenStorage(tokenEntity: TokenEntity?) : TokenStorage {

  private var inMemoryTokenEntity: TokenEntity? = tokenEntity

  override fun update(tokenEntity: TokenEntity) {
    this.inMemoryTokenEntity = tokenEntity
  }

  override fun read(): TokenEntity? {
    return this.inMemoryTokenEntity
  }

  override fun delete() {
    this.inMemoryTokenEntity = null
  }

}