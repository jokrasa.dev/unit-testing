package com.kotlintesting.stubbing

class ResolveLoggedInStatusUseCase(
  private val tokenStorage: TokenStorage
) {
  fun userLoggedIn(): Boolean {
    return tokenStorage.read() != null
  }
}