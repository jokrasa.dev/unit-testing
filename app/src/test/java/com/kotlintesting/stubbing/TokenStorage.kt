package com.kotlintesting.stubbing

/**
 * Created by jaroslawmichalik
 */
interface TokenStorage {
  fun update(tokenEntity: TokenEntity)
  fun read(): TokenEntity?
  fun delete()
}