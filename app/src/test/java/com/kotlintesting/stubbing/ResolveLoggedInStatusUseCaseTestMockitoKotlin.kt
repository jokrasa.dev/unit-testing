package com.kotlintesting.stubbing

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

/**
 * Created by jaroslawmichalik
 */
class ResolveLoggedInStatusUseCaseTestMockitoKotlin {
  @Test
  fun `it should return true on non null token`() {
    //arrange
    val useCase = ResolveLoggedInStatusUseCase(
      tokenStorage = mock {
        on { read() } doReturn TokenEntity("asd123")
      }
    )

    //act
    val userLoggedIn = useCase.userLoggedIn()

    //assert
    userLoggedIn shouldBe true
  }

  @Test
  fun `it should return false on null token`() {
    //arrange
    val useCase = ResolveLoggedInStatusUseCase(
      tokenStorage = mock {
        on { read() } doReturn null
      }
    )

    //act
    val userLoggedIn = useCase.userLoggedIn()

    //assert
    userLoggedIn shouldBe false
  }
}

