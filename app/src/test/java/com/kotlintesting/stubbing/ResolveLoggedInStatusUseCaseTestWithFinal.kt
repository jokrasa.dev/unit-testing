package com.kotlintesting.stubbing

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import org.mockito.Mockito

class ResolveLoggedInStatusUseCaseTestWithFinal {

  @Test
  fun `it should return true on non null token`() {
    //arrange
    val mockTokenStorage: SharedPreferencesTokenStorage =
      Mockito.mock(SharedPreferencesTokenStorage::class.java)

    Mockito.`when`(mockTokenStorage.read())
      .thenReturn(TokenEntity(("asd123")))

    val useCase = ResolveLoggedInStatusUseCaseFinal(
      tokenStorage = mockTokenStorage
    )

    //act
    val userLoggedIn = useCase.userLoggedIn()

    //assert
    userLoggedIn shouldBe true
  }

  @Test
  fun `it should return false on null token`() {
    //arrange
    val mockTokenStorage: SharedPreferencesTokenStorage =
      Mockito.mock(SharedPreferencesTokenStorage::class.java)

    Mockito.`when`(mockTokenStorage.read())
      .thenReturn(null)

    val useCase = ResolveLoggedInStatusUseCaseFinal(
      tokenStorage = mockTokenStorage
    )

    //act
    val userLoggedIn = useCase.userLoggedIn()

    //assert
    userLoggedIn shouldBe false
  }
}