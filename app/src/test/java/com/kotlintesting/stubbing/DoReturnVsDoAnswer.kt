package com.kotlintesting.stubbing

import org.junit.jupiter.api.Test
import org.mockito.kotlin.doAnswer
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import kotlin.random.Random

/**
 * Created by jaroslawmichalik
 */
class DoReturnVsDoAnswer {
  @Test
  fun `it should return`() {
    val timeProvider = mock<RandomNumberProvider> {
      on { random() } doReturn Random.nextInt()
    }

    repeat(10) {
      println("Return " + timeProvider.random())
    }
  }

  @Test
  fun `it should answer`() {
    val timeProvider = mock<RandomNumberProvider> {
      on { random() } doAnswer {
        Random.nextInt()
      }
    }

    repeat(10) {
      println("Answer " + timeProvider.random())
    }
  }
}

interface RandomNumberProvider {
  fun random(): Int
}