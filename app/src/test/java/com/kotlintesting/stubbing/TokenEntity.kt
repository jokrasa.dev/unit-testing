package com.kotlintesting.stubbing

data class TokenEntity(val token: String)