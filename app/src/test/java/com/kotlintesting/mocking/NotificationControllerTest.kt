package com.kotlintesting.mocking

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.never
import org.mockito.kotlin.verify

/**
 * Created by jaroslawmichalik
 */
class NotificationControllerTest{
  @Test
  fun `given LAST_MINUTE_PROMO type when message arrives then display notification`(){
    //arrange
    val givenMessage = mapOf(
      "TYPE" to "LAST_MINUTE_PROMO",
      "TITLE" to "1 hour remaining",
      "SUBTITLE" to "Opel Astra on sale",
      "PRODUCT_ID" to "opel-astra-123"
    )
    val renderer: NotificationRenderer = mock()

    val controller = NotificationController(
      renderer = renderer
    )

    controller.messageReceived(givenMessage)

    verify(renderer).display(any(), any(), any())
  }

  @Test
  fun `given PING type when message arrives then don't display anything`(){
    //arrange
    val givenMessage = mapOf(
      "TYPE" to "PING"
    )
    val renderer: NotificationRenderer = mock()

    val controller = NotificationController(
      renderer = renderer
    )

    controller.messageReceived(givenMessage)

    verify(renderer, never()).display(any(), any(), any())
  }
}