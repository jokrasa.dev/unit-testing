package com.kotlintesting.mocking

/**
 * Created by jaroslawmichalik
 */
class NotificationController(private val renderer: NotificationRenderer) {
  fun messageReceived(message: Map<String, String>) {
    if(message.isLastMinutePromo()){
      renderer.display(
        title = message.resolveTitle(),
        subtitle = message.resolveSubtitle(),
        deepLink = message.buildDeepLink()
      )
    }
    else {
      // do nothing
    }
  }
private fun Map<String, String>.isLastMinutePromo(): Boolean {
  return this["TYPE"] == "LAST_MINUTE_PROMO"
}

  private fun Map<String, String>.resolveTitle(): String {
    return this["TITLE"].orEmpty()
  }

  private fun Map<String, String>.resolveSubtitle(): String {
    return this["SUBTITLE"].orEmpty()
  }

  private fun Map<String, String>.buildDeepLink(): String {
    val productId = this["PRODUCT_ID"].orEmpty()

    return "app://product?id=$productId"
  }
}


interface NotificationRenderer {
  fun display(title: String, subtitle: String, deepLink: String)
}